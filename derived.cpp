// Alireza Rezaeinia

#include <iostream>
#include <queue>
#include <string>
#include <cmath>
#include <algorithm>
#include <vector>
#include <ctime>
#include <sstream>
#include <set>
#include <cstdio>
#include <functional>
#include <map>
#include <iomanip>
#include <fstream>
#include <iterator>


using namespace std;
typedef long long ll;
#define read freopen("a.in" , "r" , stdin);
#define write freopen("b.out" , "w" , stdout);
#define pii pair<ll ,ll>

bool visited[10000] = { false };
bool F = false;
bool u = false;
string currentmosh = "", current = "", amalgar = "^*/+-sctql";
bool makhsefr = false;
ll h = -1;
ll g = 0;

inline int check(int q = 0)
{
	static int f = 0;
	if (q == 0)
	{
		for (int i = 0; i < currentmosh.length(); i++)
		{
			if (currentmosh[i] == '(')
			{
				int k = 1;
				for (int j = i + 1; ; j++)
				{
					if (currentmosh[j] == '(')
						k++;
					if (currentmosh[j] == ')')
						k--;
					if (k == 0)
					{
						i = j;
						break;
					}
				}
				continue;
			}
			if (currentmosh[i] == '+' || currentmosh[i] == '-' || currentmosh[i] == '*' || currentmosh[i] == '/' || currentmosh[i] == '^')
				f++;
		}
	}
	if (f == 1 || f == 0)
	{
		f = 0;
		return 0;
	}
	if (q == 0)
	{
		for (int i = currentmosh.length() - 1; i >= 0; i--)
		{
			if (currentmosh[i] == ')')
			{
				int k = 1;
				for (int j = i - 1; ; j--)
				{
					if (currentmosh[j] == ')')
						k++;
					if (currentmosh[j] == '(')
						k--;
					if (k == 0)
					{
						i = j;
						break;
					}
				}
				continue;
			}
			if (currentmosh[i] == '^')
			{
				string a = "", b = "" ,hole="";
				int k1 = 0, k2 = 0;
				int ee = 0;
				for (int l = i + 1; l < currentmosh.length(); l++)
				{
					if (currentmosh[l] == '(')
						k1++;
					if (currentmosh[l] == ')')
						k1--;
					a += currentmosh[l];
					if (k1 == 0)
						break;
				}
				for (int l = i - 1; l >= 0; l--)
				{
					ee = l;
					if (currentmosh[l] == ')')
						k2++;
					if (currentmosh[l] == '(')
						k2--;
					b += currentmosh[l];
					if (k2 == 0)
						break;
				}
				reverse(b.begin(), b.end());
				hole += '(';
				hole += b;
				hole += '^';
				hole += a;
				hole += ')';
				currentmosh.replace(ee, hole.length() - 2, hole);
				g += 2;
				i -= b.length()-1;
				f--;
				if (f == 1)
				{
					f = 0;
					return 0;
				}
			}
		}
	}
	else
	{
		for (int i = 0; i < currentmosh.length(); i++)
		{
			if (currentmosh[i] == '(')
			{
				int k = 1;
				for (int j = i + 1; ; j++)
				{
					if (currentmosh[j] == '(')
						k++;
					if (currentmosh[j] == ')')
						k--;
					if (k == 0)
					{
						i = j;
						break;
					}
				}
				continue;
			}
			if (q == 1)
			{
				if (currentmosh[i] == '*' || currentmosh[i] == '/')
				{
					string a = "", b = "", hole = "";
					int k1 = 0, k2 = 0;
					int ee = 0;
					for (int l = i + 1; l < currentmosh.length(); l++)
					{
						if (currentmosh[l] == '(')
							k1++;
						if (currentmosh[l] == ')')
							k1--;
						a += currentmosh[l];
						if (k1 == 0)
							break;
					}
					for (int l = i - 1; l >= 0; l--)
					{
						ee = l;
						if (currentmosh[l] == ')')
							k2++;
						if (currentmosh[l] == '(')
							k2--;
						b += currentmosh[l];
						if (k2 == 0)
							break;
					}
					reverse(b.begin(), b.end());
					hole += '(';
					hole += b;
					hole += currentmosh[i];
					hole += a;
					hole += ')';
					currentmosh.replace(ee, hole.length() - 2, hole);
					g += 2;
					i += a.length() + 1;
					f--;
					if (f == 1)
					{
						f = 0;
						return 0;
					}
				}
			}
			if (q == 2)
			{
				if (currentmosh[i] == '+' || currentmosh[i] == '-')
				{
					string a = "", b = "", hole = "";
					int k1 = 0, k2 = 0;
					int ee = 0;
					for (int l = i + 1; l < currentmosh.length(); l++)
					{
						if (currentmosh[l] == '(')
							k1++;
						if (currentmosh[l] == ')')
							k1--;
						a += currentmosh[l];
						if (k1 == 0)
							break;
					}
					for (int l = i - 1; l >= 0; l--)
					{
						ee = l;
						if (currentmosh[l] == ')')
							k2++;
						if (currentmosh[l] == '(')
							k2--;
						b += currentmosh[l];
						if (k2 == 0)
							break;
					}
					reverse(b.begin(), b.end());
					hole += '(';
					hole += b;
					hole += currentmosh[i];
					hole += a;
					hole += ')';
					currentmosh.replace(ee, hole.length() - 2, hole);
					g += 2;
					i += a.length() + 1;
					f--;
					if (f == 1)
					{
						f = 0;
						return 0;
					}
				}
			}
		}
	}
	return check(q + 1);
}

inline int parenthesis(string &currentmosh, int q)
{
	int t = 0;
	int fa, fb;
	bool a = false, b = false;
	if (q == 3)
		return 0;
	if (q == 0)
	{
		for (int i = 0; i < currentmosh.length(); i++)
		{
			if (currentmosh[i] == '(')
			{
				int k = 1;
				for (int z = i + 1; ; z++)
				{
					if (currentmosh[z] == '(')
						k++;
					if (currentmosh[z] == ')')
						k--;
					if (k == 0)
					{
						i = z;
						break;
					}
				}
				continue;
			}
			if (currentmosh[i] == '-' || currentmosh[i] == '+')
			{
				if (i ==0  || currentmosh[i - 1] == '+' || currentmosh[i - 1] == '-' || currentmosh[i - 1] == '*' || currentmosh[i - 1] == '/' || currentmosh[i - 1] == '^' || currentmosh[i - 1] == 's' || currentmosh[i - 1] == 'c' || currentmosh[i - 1] == 'l' || currentmosh[i - 1] == 'q' || currentmosh[i - 1] == 't')
				{
					int y = 0;
					y++;
					string ue = "(";
					ue += currentmosh[i];
					for (int qq = i + 1 ; currentmosh[qq] !='\0' && currentmosh[qq] !='+' && currentmosh[qq] != '-' && currentmosh[qq] != '*' && currentmosh[qq] != '/' && currentmosh[qq] != '^'; qq++)
					{
						if (currentmosh[qq] == '(')
						{
							int k = 1;
							y++;
							ue += currentmosh[qq];
							for (int h = qq + 1; ; h++)
							{
								ue += currentmosh[h];
								if (currentmosh[h] == '(')
									k++;
								if (currentmosh[h] == ')')
									k--;
								y++;
								if (k == 0)
								{
									qq = h;
									break;
								}
							}
							continue;
						}
						ue += currentmosh[qq];
						y++;
					}
					ue += ')';
					currentmosh.replace(i, y, ue);
					g += 2;
					i += y;
				}
			}
		}
	}
	if (q == 1)
	{
		for (int i = currentmosh.length()-1; i >= 0; i--)
		{
			//if (currentmosh[i] == ')')
			//{
			//	int k = 1;
			//	for (int h = i-1; ; h--)
			//	{
			//		if (currentmosh[h] == ')')
			//			k++;
			//		if (currentmosh[h] == '(')
			//			k--;
			//		if (k == 0)
			//		{
			//			i = h;
			//			break;
			//		}
			//	}
			//}
			if (currentmosh[i] == 'l' || currentmosh[i] == 's' || currentmosh[i] == 'c' || currentmosh[i] == 't' || currentmosh[i] == 'q' || currentmosh[i] == 'j')
			{
				int y = 0;
				string ca = "";
				string eplus = "";
				for (int o = i + 1; ; o++)
				{
					y++;
					if (y == 1 && currentmosh[o] == '(')
					{
						int k = 1;
						string e = "(";
						for (int q = o + 1; ; q++)
						{
							e += currentmosh[q];
							if (currentmosh[q] == '(')
								k++;
							if (currentmosh[q] == ')')
								k--;
							y++;
							if (k == 0)
								break;
						}
						eplus = e;
						break;
					}
					if (currentmosh[o]=='\0' ||currentmosh[o] == '^' || currentmosh[o] == '*' || currentmosh[o] == '/' || currentmosh[o] == '+' || currentmosh[o] == '-' || currentmosh[o] == ')')
					{
						y--;
						string u = "(";
						u += ca;
						u += ")";
						ca = u;
						g += 2;
						break;
					}
					ca += currentmosh[o];
				}
				if (i == 0)
				{
					string r = "(";
					r += currentmosh[i];
					if (ca != "\0")
						r += ca;
					else
						r += eplus;
					r += ")";
					currentmosh.replace(i, y+1, r);
					g += 2;
					continue;
				}
				else if (currentmosh[i - 1] != '(' )
				{
					if (i > 1 && (currentmosh[i - 1] == '+' || currentmosh[i - 1] == '-') && currentmosh[i - 2] == '(')
					{

					}
					else
					{
						string r = "(";
						r += currentmosh[i];
						if (ca != "\0")
							r += ca;
						else
							r += eplus;
						r += ")";
						currentmosh.replace(i, y + 1, r);
						g += 2;
						continue;
					}
				}
				if (!ca.empty())
					currentmosh.replace(i + 1, y, ca);
			}
		}
	}
	else if (q == 2)
	{
		for (int i = 0; i < currentmosh.length(); i++)
		{
			if (currentmosh[i] == '(')
			{
				t++;
				for (int j = i; j < currentmosh.length(); j++)
				{
					if (currentmosh[j] == ')')
						t--;
					if (t == 0)
					{
						if (currentmosh[j + 1] != '\0')
							i = j + 1;
						else
							i = j;
						break;
					}
				}
			}
			if (currentmosh[i] == '^' || currentmosh[i] == '*' || currentmosh[i] == '/' || currentmosh[i] == '+' || currentmosh[i] == '-')
			{
				int p = i, s = i;
				for (int j = i; currentmosh[j] != '\0' && currentmosh[j] != '(' && currentmosh[j] != '*' && currentmosh[j] != '/' && currentmosh[j] != '+' && currentmosh[j] != '-'; j++)
				{
					if (currentmosh[j] == '^' || currentmosh[j] == '*' || currentmosh[j] == '/' || currentmosh[j] == '+' || currentmosh[j] == '-')
						p = j;
				}
				i = p;
				for (int j = p; j >= s; j--)
				{
					if (currentmosh[j] == ')')
					{
						t++;
						for (int k = j; k >= 0; k--)
						{
							if (currentmosh[k] == '(')
								t--;
							if (t == 0)
							{
								if (k - 1 != -1)
									j = k - 1;
								else
									j = k;
								break;
							}
						}
					}
					if (currentmosh[j] == '^' || currentmosh[j] == '*' || currentmosh[j] == '/' || currentmosh[j] == '+' || currentmosh[j] == '-')
					{
						string catavan = "(", cbtavan = "";
						for (int k = j + 1; k <= currentmosh.length(); k++)
						{
							if (k == currentmosh.length())
							{
								fa = k - 1;
								break;
							}
							if (currentmosh[k] == '(' || currentmosh[k] == '*' || currentmosh[k] == '/' || currentmosh[k] == '+' || currentmosh[k] == '-' || currentmosh[k] == '^')
							{
								if (currentmosh[k] == '(')
									a = true;
								fa = k - 1;
								break;
							}
							catavan += currentmosh[k];
						}
						for (int k = j - 1; k >= -1; k--)
						{
							if (k == -1)
							{
								fb = k + 1;
								break;
							}
							if (currentmosh[k] == ')' || currentmosh[k] == '^' || currentmosh[k] == '/' || currentmosh[k] == '+' || currentmosh[k] == '-' || currentmosh[k] == '*')
							{
								if (currentmosh[k] == ')')
									b = true;
								fb = k + 1;
								break;
							}
							cbtavan += currentmosh[k];
						}
						if (cbtavan != "\0")
						{
							string g = "(";
							reverse(cbtavan.begin(), cbtavan.end());
							g += cbtavan;
							cbtavan = g;
						}
						if (a == true && b == true)
						{
							a = false;
							b = false;
							continue;
						}
						else if (b == true)
						{
							b = false;
							catavan += ')';
							currentmosh.replace(j + 1, fa - j, catavan);
							g += 2;
						}
						else if (a == true)
						{
							a = false;
							cbtavan += ')';
							currentmosh.replace(fb, j - fb, cbtavan);
							g += 2;
							j += 2;
							i += 2;
						}
						else
						{
							if (j == i)
								i += 2;
							else
								i += 4;
							catavan += ')';
							cbtavan += ')';
							currentmosh.replace(j + 1, fa - j, catavan);
							currentmosh.replace(fb, j - fb, cbtavan);
							j += 2;
							g += 4;
						}
					}
				}
			}
		}
	}
	return parenthesis(currentmosh, q + 1);
}

inline int Catch_parentheses(string &mosh, int i)
{
	if (i == mosh.length())
		return 0;
	if (mosh[i] == '(' && F && !visited[i])
	{
		h = i;
		F = false;
		visited[i] = true;
		return i;
	}
	if (F)
	{
		currentmosh += mosh[i];
		return Catch_parentheses(mosh, i - 1);
	}
	if (mosh[i] == ')' && !visited[i])
	{
		F = true;
		Catch_parentheses(mosh, i - 1);
		reverse(currentmosh.begin(), currentmosh.end());
		int q = currentmosh.length();
		parenthesis(currentmosh, 0);
		check();
		mosh.replace(h + 1, q, currentmosh);
		i += g;
		g = 0;
		for (int b = h; b <= i; b++)
			visited[b] = true;
		//cout << currentmosh << endl;
		currentmosh.clear();
	}
	Catch_parentheses(mosh, i + 1);
	return 0;
}
string moshtagh(string mosh)
{
	int k = 0;
	string center = "";
	bool ifparantez2 = false;
	for (int i = 0; i < mosh.length(); i++)
	{
		if (i == 0 && mosh[i] != '(')
			break;
		if (mosh[i] == '(')
			k++;
		if (mosh[i] == ')')
			k--;
		if (i != 0 && i != mosh.length()-1)
			center += mosh[i];
		if (k == 0 && i == mosh.length() - 1)
			return moshtagh(center);
		if (k == 0)
			break;
	}
	if (mosh.find("x") == -1)
		return "0";
	if (mosh == "-x")
		return "(-1)";
	else if (mosh == "+x" || mosh == "x")
		return "1";
	if (mosh[0] == '+' || mosh[0] == '-')
	{
		if (mosh[1] == 's')
		{
			string U = "" , hole="";
			for (int i = 2; i < mosh.length(); i++)
			{
				if (mosh[i] == '(')
				{
					U += mosh[i];
					int p = 1;
					for (int v = i + 1;; v++)
					{
						if (mosh[v] == '(')
							p++;
						if (mosh[v] == ')')
							p--;
						U += mosh[v];
						if (p == 0)
							break;
					}
					break;
				}
			}
			string dU = moshtagh(U);
			if (mosh[0] == '-')
				hole += "(-" + dU + "*" + "c" + U + ")";
			else
				hole += "(" + dU + "*" + "c" + U + ")";
			return hole;
		}
		if (mosh[1] == 'c')
		{
			string U = "", hole = "";
			for (int i = 2; i < mosh.length(); i++)
			{
				if (mosh[i] == '(')
				{
					U += mosh[i];
					int p = 1;
					for (int v = i + 1;; v++)
					{
						if (mosh[v] == '(')
							p++;
						if (mosh[v] == ')')
							p--;
						U += mosh[v];
						if (p == 0)
							break;
					}
					break;
				}
			}
			string dU = moshtagh(U);
			if (mosh[0] == '-')
				hole += "(" + dU + "*" + "s" + U + ")";
			else
				hole += "(-" + dU + "*" + "s" + U + ")";
			return hole;
		}
		if (mosh[1] == 't')
		{
			string U = "", hole = "";
			for (int i = 2; i < mosh.length(); i++)
			{
				if (mosh[i] == '(')
				{
					U += mosh[i];
					int p = 1;
					for (int v = i + 1;; v++)
					{
						if (mosh[v] == '(')
							p++;
						if (mosh[v] == ')')
							p--;
						U += mosh[v];
						if (p == 0)
							break;
					}
					break;
				}
			}
			string dU = moshtagh(U);
			if (mosh[0] == '-')
				hole += "(-" + dU + "*" + "(1+t^2" + U + "))";
			else
				hole += "(" + dU + "*" + "(1+t^2" + U + "))";
			return hole;
		}
		if (mosh[1] == 'q')
		{
			string U = "", hole = "";
			for (int i = 2; i < mosh.length(); i++)
			{
				if (mosh[i] == '(')
				{
					U += mosh[i];
					int p = 1;
					for (int v = i + 1;; v++)
					{
						if (mosh[v] == '(')
							p++;
						if (mosh[v] == ')')
							p--;
						U += mosh[v];
						if (p == 0)
							break;
					}
					break;
				}
			}
			string dU = moshtagh(U);
			if (mosh[0] == '-')
				hole += "(" + dU + "*" + "(1+q^2" + U + "))";
			else
				hole += "(-" + dU + "*" + "(1+q^2" + U + "))";
			return hole;
		}
		if (mosh[1] == 'j')
		{
			string U = "", hole = "" , tavan="";
			for (int i = 2; i < mosh.length(); i++)
			{
				if (mosh[i] == '(')
				{
					U += mosh[i];
					int p = 1;
					for (int v = i + 1;; v++)
					{
						if (mosh[v] == '(')
							p++;
						if (mosh[v] == ')')
							p--;
						U += mosh[v];
						if (p == 0)
						{
							i = v;
							break;
						}
					}
				}
			}
			string dU = moshtagh(U);
			if (mosh[0] == '-')
				hole += "(-(" + dU + "/(2*j" + U + ")))";
			else
					hole += "(" + dU + "/(2*j" + U + "))";
			return hole;
		}
		if (mosh[1] == 'l')
		{
			string U = "", hole = "";
			for (int i = 2; i < mosh.length(); i++)
			{
				if (mosh[i] == '(')
				{
					U += mosh[i];
					int p = 1;
					for (int v = i + 1;; v++)
					{
						if (mosh[v] == '(')
							p++;
						if (mosh[v] == ')')
							p--;
						U += mosh[v];
						if (p == 0)
							break;
					}
					break;
				}
			}
			string dU = moshtagh(U);
			if (mosh[0] == '-')
				hole += "(-(" + dU + "/" + U + "))";
			else
				hole += "(" + dU + "/" + U + ")";
			return hole;
		}
	}
	if (mosh[0] == 's')
	{
		string U = "", hole = "";
		for (int i = 1; i < mosh.length(); i++)
		{
			if (mosh[i] == '(')
			{
				U += mosh[i];
				int p = 1;
				for (int v = i + 1;; v++)
				{
					if (mosh[v] == '(')
						p++;
					if (mosh[v] == ')')
						p--;
					U += mosh[v];
					if (p == 0)
						break;
				}
				break;
			}
		}
		string dU = moshtagh(U);
		hole += "(" + dU + "*" + "c" + U + ")";
		return hole;
	}
	if (mosh[0] == 'c')
	{
		string U = "", hole = "";
		for (int i = 1; i < mosh.length(); i++)
		{
			if (mosh[i] == '(')
			{
				U += mosh[i];
				int p = 1;
				for (int v = i + 1;; v++)
				{
					if (mosh[v] == '(')
						p++;
					if (mosh[v] == ')')
						p--;
					U += mosh[v];
					if (p == 0)
						break;
				}
				break;
			}
		}
		string dU = moshtagh(U);
		hole += "(-" + dU + "*" + "s" + U + ")";
		return hole;
	}
	if (mosh[0] == 't')
	{
		string U = "", hole = "";
		for (int i = 1; i < mosh.length(); i++)
		{
			if (mosh[i] == '(')
			{
				U += mosh[i];
				int p = 1;
				for (int v = i + 1;; v++)
				{
					if (mosh[v] == '(')
						p++;
					if (mosh[v] == ')')
						p--;
					U += mosh[v];
					if (p == 0)
						break;
				}
				break;
			}
		}
		string dU = moshtagh(U);
		hole += "(" + dU + "*" + "(1+t^2" + U + "))";
		return hole;
	}
	if (mosh[0] == 'q')
	{
		string U = "", hole = "";
		for (int i = 1; i < mosh.length(); i++)
		{
			if (mosh[i] == '(')
			{
				U += mosh[i];
				int p = 1;
				for (int v = i + 1;; v++)
				{
					if (mosh[v] == '(')
						p++;
					if (mosh[v] == ')')
						p--;
					U += mosh[v];
					if (p == 0)
						break;
				}
				break;
			}
		}
		string dU = moshtagh(U);
		hole += "(-" + dU + "*" + "(1+q^2" + U + "))";
		return hole;
	}
	if (mosh[0] == 'j')
	{
		string U = "", hole = "";
		for (int i = 1; i < mosh.length(); i++)
		{
			if (mosh[i] == '(')
			{
				U += mosh[i];
				int p = 1;
				for (int v = i + 1;; v++)
				{
					if (mosh[v] == '(')
						p++;
					if (mosh[v] == ')')
						p--;
					U += mosh[v];
					if (p == 0)
					{
						i = v;
						break;
					}
				}
			}
		}
		string dU = moshtagh(U);
		hole += "(" + dU + "/(2*j" + U + "))";
		return hole;
	}
	if (mosh[0] == 'l')
	{
		string U = "", hole = "";
		for (int i = 1; i < mosh.length(); i++)
		{
			if (mosh[i] == '(')
			{
				U += mosh[i];
				int p = 1;
				for (int v = i + 1;; v++)
				{
					if (mosh[v] == '(')
						p++;
					if (mosh[v] == ')')
						p--;
					U += mosh[v];
					if (p == 0)
						break;
				}
				break;
			}
		}
		string dU = moshtagh(U);
		hole += "(" + dU + "/" + U + ")";
		return hole;
	}
	for (int i = 0; i < mosh.length(); i++)
	{
		if (mosh[i] == '(')
		{
			int k2 = 1;
			for (int j = i + 1; j < mosh.length(); j++)
			{
				if (mosh[j] == '(')
					k2++;
				if (mosh[j] == ')')
					k2--;
				if (k2 == 0)
				{
					i = j;
					break;
				}
			}
		}
		if (mosh[i] == '+' || mosh[i] == '-' || mosh[i] == '*' || mosh[i] == '/' || mosh[i] == '^')
		{
			string before = "", after = "";
			for (int j = i + 1; j < mosh.length(); j++)
				after += mosh[j];
			for (int j = 0; j < i; j++)
				before += mosh[j];
			if (mosh[i] == '+')
			{
				string difbefore = moshtagh(before);
				string difafter = moshtagh(after);
				if (difafter[0] == '(')
				{
					difafter.erase(0, 1);
					difafter.erase(difafter.length()-1, 1);
				}
				if (difbefore[0] == '(')
				{
					difbefore.erase(0, 1);
					difbefore.erase(difbefore.length()-1, 1);
				}
				string hole;
				if (difafter == "0")
					 hole = "(" + difbefore + ")";
				else if (difbefore == "0")
					hole = "(" + difafter + ")";
				else
					hole = "(" + difbefore + "+" + difafter + ")";
				return hole;
			}
			if (mosh[i] == '-')
			{
				string difbefore = moshtagh(before);
				string difafter = moshtagh(after);
				if (difafter[0] == '(')
				{
					difafter.erase(0, 1);
					difafter.erase(difafter.length() - 1, 1);
				}
				if (difbefore[0] == '(')
				{
					difbefore.erase(0, 1);
					difbefore.erase(difbefore.length() - 1, 1);
				}
				string hole;
				if (difafter == "0")
					hole = "(" + difbefore + ")";
				else if (difbefore == "0")
					hole = "-(" + difafter + ")";
				else
					hole = "(" + difbefore + "-" + difafter + ")";
				return hole;
			}
			if (mosh[i] == '*')
			{
				after.erase(0, 1);
				after.erase(after.length() - 1, 1);
				before.erase(0, 1);
				before.erase(before.length() - 1, 1);
				string difbefore = moshtagh(before);
				string difafter = moshtagh(after);
				string hole = "";
				if (difafter[0] == '(')
				{
					if (difafter.find("/") == -1 && difafter.find("+") == -1 && difafter.find("-") == -1)
					{

						difafter.erase(0, 1);
						difafter.erase(difafter.length() - 1, 1);
					}
				}
				if (difbefore[0] == '(')
				{
					if (difbefore.find("/") == -1 && difbefore.find("+") == -1 && difbefore.find("-") == -1)
					{

						difbefore.erase(0, 1);
						difbefore.erase(difbefore.length() - 1, 1);
					}
				}
				if (difbefore == "0" || after == "0")
				{
					if (difafter == "0" || before == "0")
						hole = "0";
					else if (difafter == "1")
					{
						if ( before.find("/") == -1 && before.find("+") == -1 && before.find("-") == -1)
							hole = before;
						else
							hole = "(" + before + ")";
					}
					else if (before == "1")
					{
						hole = "(" + difafter + ")";
					}
					else
					{
						if (before.find("/") == -1 && before.find("+") == -1 && before.find("-") == -1)
							hole = "(" + difafter + "*" + before + ")";
						else
							hole = "(" + difafter + "*(" + before + "))";
					}
				}
				else if (difafter == "0" || before == "0")
				{
					if (difbefore == "0" || after == "0")
						hole = "0";
					else if (difbefore == "1")
					{
						if (after.find("^") == -1 && after.find("*") == -1 && after.find("/") == -1 && after.find("+") == -1 && after.find("-") == -1)
						hole = after ;
						else
							hole = "(" + after + ")";
					}
					else if (after == "1")
					{
						hole = "(" + difbefore + ")";
					}
					else
					{
						if (after.find("^") == -1 && after.find("*") == -1 && after.find("/") == -1 && after.find("+") == -1 && after.find("-") == -1)
							hole = "(" + difbefore + "*" + after + ")";
						else
							hole = "(" + difbefore + "*(" + after + "))";
					}
				}
				else if (difafter == "1" && difbefore == "1")
				{
					if (after.find("/") == -1 && after.find("+") == -1 && after.find("-") == -1 && before.find("/") == -1 && before.find("+") == -1 && before.find("-") == -1)
						hole = "(" + before + "+" + after + ")";
					else if( after.find("/") == -1 && after.find("+") == -1 && after.find("-") == -1)
						hole = "((" + before + ")+" + after + ")";
					else if ( before.find("/") == -1 && before.find("+") == -1 && before.find("-") == -1)
						hole = "(" + before + "+(" + after + "))";
					else
						hole = "((" + before + ")+(" + after + "))";
				}
				else if (difafter == "1" && after == "1")
				{

					if (before.find("/") == -1 && before.find("+") == -1 && before.find("-") == -1)
						hole = "(" + before + "+" + difbefore + ")";
					else
						hole = "((" + before + ")+" + difbefore + ")";
				}
				else if (before == "1" && after == "1")
					hole = "(" + difafter + "+" + difbefore +  ")";
				else if (before == "1" && difbefore == "1")
				{
					if (after.find("/") == -1 && after.find("+") == -1 && after.find("-") == -1)
						hole = "(" + difafter + "+" + after + ")";
					else
						hole = "(" + difafter + "+(" + after + "))";
				}
				else if (difafter == "1")
				{
					if ( after.find("/") == -1 && after.find("+") == -1 && after.find("-") == -1 &&  before.find("/") == -1 && before.find("+") == -1 && before.find("-") == -1)
						hole = "(" + before + "+" + difbefore + "*" + after + ")";
					else if (after.find("/") == -1 && after.find("+") == -1 && after.find("-") == -1)
						hole = "((" + before + ")+" + difbefore + "*" + after + ")";
					else if ( before.find("/") == -1 && before.find("+") == -1 && before.find("-") == -1)
						hole = "(" + before + "+" + difbefore + "*(" + after + "))";
					else
						hole = "((" + before + ")+" + difbefore + "*(" + after + "))";
				}
				else if (before == "1")
				{
					if (after.find("/") == -1 && after.find("+") == -1 && after.find("-") == -1 )
						hole = "(" + difafter + "+" + difbefore + "*" + after + ")";
					else
						hole = "(" + difafter + "+" + difbefore + "*(" + after + "))";
				}
				else if (difbefore == "1")
				{
					if ( after.find("/") == -1 && after.find("+") == -1 && after.find("-") == -1 && before.find("/") == -1 && before.find("+") == -1 && before.find("-") == -1)
						hole = "(" + difafter + "*" + before + "+" + after + ")";
					else if ( after.find("/") == -1 && after.find("+") == -1 && after.find("-") == -1)
						hole = "(" + difafter + "*(" + before + ")+" + after + ")";
					else if ( before.find("/") == -1 && before.find("+") == -1 && before.find("-") == -1)
						hole = "(" + difafter + "*" + before + "+(" + after + "))";
					else
						hole = "(" + difafter + "*(" + before + ")+(" + after + "))";
				}
				else if (after == "1")
				{
					 if ( before.find("/") == -1 && before.find("+") == -1 && before.find("-") == -1)
						hole = "(" + difafter + "*" + before + "+" + difbefore + ")";
					 else
						 hole = "(" + difafter + "*(" + before + ")+" + difbefore + ")";
				}
				else
				{
					if (after.find("/") == -1 && after.find("+") == -1 && after.find("-") == -1 && before.find("/") == -1 && before.find("+") == -1 && before.find("-") == -1)
						hole = "(" + difafter + "*" + before + "+" + difbefore + "*" + after + ")";
					else if (after.find("/") == -1 && after.find("+") == -1 && after.find("-") == -1)
						hole = "(" + difafter + "*(" + before + ")+" + difbefore + "*" + after + ")";
					else if (before.find("/") == -1 && before.find("+") == -1 && before.find("-") == -1)
						hole = "(" + difafter + "*" + before + "+" + difbefore + "*(" + after + "))";
					else
						hole = "(" + difafter + "*(" + before + ")+" + difbefore + "*(" + after + "))";
				}
				return hole;
			}
			if (mosh[i] == '/')
			{
				after.erase(0, 1);
				after.erase(after.length() - 1, 1);
				before.erase(0, 1);
				before.erase(before.length() - 1, 1);
				string difbefore = moshtagh(before);
				string difafter = moshtagh(after);
				if (after == "0")
				{
					makhsefr = true;
					return "0";
				}
				string hole = "((("+difbefore + "*(" + after +"))-(" +  difafter + "*(" + before + ")))/((" + after + ")^(2)))";
				return hole;
			}
			if (mosh[i] == '^')
			{
				if (before == "(e)")
				{
					if (after.find("x") == -1)
					{
						string hole = "0";
						return hole;
					}
					else
					{
						string hole;
						string difafter = moshtagh(after);
						if (difafter == "1" || difafter == "(1)")
							hole = "(e^"+after +")";
						else
							hole = "(" + difafter + "*" + before + "^" + after +")";
						return hole;
					}
				}
				if (before.find("x") == -1)
				{
					if (after.find("x") == -1)
					{
						string hole = "0";
						return hole;
					}
					else
					{
						string difafter = moshtagh(after);
						string hole = "(" + difafter + "*"  + before + "^" + after + "*" + "l" + before + ")";
						return hole;
					}
				}
				else
				{
					string dbefore = moshtagh(before);
					if (after.find('x') == -1)
					{
						if (after == "(0)")
						{
							string hole = "0";
							return hole;
						}
						if (after == "(1)")
						{
							string hole = "(" + dbefore +  ")";
							return hole;
						}
						else
						{
							string dbefore = moshtagh(before);
							string tavan_ = "";
							long double tavan;
							after.erase(0, 1);
							after.erase(after.length()-1, 1);
							if (after[0] == '+' || after[0] == '-')
							{
								if (isdigit(after[1])) {
									tavan = stold(after);
									tavan--;
									tavan_ = to_string(tavan);
								}
								else
								{
									tavan_ = "(" + after + ") - (1)";
								}
							}
							else
							{
								if (isdigit(after[0])) {
									tavan = stold(after);
									tavan--;
									tavan_ = to_string(tavan);
								}
								else
								{
									tavan_ = "(" + after + ") - (1)";
								}
							}
							
							int isdecimal;
							isdecimal = tavan_.find('.');
							if (isdecimal != -1)
							{
								for (int i = tavan_.length() - 1; isdecimal <= i ;i--)
								{
									if (tavan_[i] == '0')
										tavan_.erase(i, 1);
									else if (i == isdecimal)
										tavan_.erase(i, 1);
									else
										break;
								}
							}
							string hole = "((" + after + ")*" + before + "^(" + tavan_ + ")" + "*" + dbefore +  ")";
							return hole;
						}
					}
					else
					{
						string dafter = moshtagh(after);
						string dbefore = moshtagh(before);
						string hole = "(" + before + "^" + after + "*(" + dafter + "*l" + before  + "+" + dbefore + "*" +after +  "/" + before + "))" ;
						return hole;
					}
				}
			}
		}
	}
}

int main()
{
	int sin = 0, cos = 0, space = 0, ln = 0, tan = 0, cot = 0, sqrt = 0, k = 0 ;
	string mosh, The_final_answer ;
	
	getline(cin, mosh);
	for (int i = 0; i < mosh.length(); i++)
	{
		if ((mosh[i] == 'x' || mosh[i] == 'e') && i != 0)
		{
			if (isdigit(mosh[i - 1]))
				mosh.insert(i , "*");
		}

		if (isalpha(mosh[i]))
			mosh[i] = tolower(mosh[i]);
	}
	for (int i = 0; i < mosh.length(); i++)
	{
		if (mosh[i] == '(')
			k++;
		if (mosh[i] == ')')
			k--;
		if (k == 0)
		{
			if (i != mosh.length() - 1 || mosh.length() == 1)
			{
				string a = "";
				a += '(';
				a += mosh;
				a += ')';
				mosh = a;
			}
			break;
		}
	}
	while (sin != -1 || cos != -1 || space != -1 || ln != -1 || tan != -1 || cot != -1 || sqrt != -1)
	{
		sin = mosh.find("sin");
		if (sin != -1)
			mosh.replace(sin, 3, "s");
		cos = mosh.find("cos");
		if (cos != -1)
			mosh.replace(cos, 3, "c");
		tan = mosh.find("tan");
		if (tan != -1)
			mosh.replace(tan, 3, "t");
		cot = mosh.find("cot");
		if (cot != -1)
			mosh.replace(cot, 3, "q");
		ln = mosh.find("ln");
		if (ln != -1)
			mosh.replace(ln, 2, "l");
		space = mosh.find(' ');
		if (space != -1)
			mosh.erase(space, 1);
		sqrt = mosh.find("sqrt");
		if (sqrt != -1)
			mosh.replace(sqrt, 4, "j");
	}
	Catch_parentheses(mosh, 0);
	The_final_answer = moshtagh(mosh);
	for (int i = 0; i < The_final_answer.length(); i++)
	{
		if (The_final_answer[i] == 's')
		{
			The_final_answer.replace(i, 1, "sin");
			i += 2;
		}
		if (The_final_answer[i] == 'c')
		{
			The_final_answer.replace(i, 1, "cos");
			i += 2;
		}
		if (The_final_answer[i] == 't')
		{
			The_final_answer.replace(i, 1, "tan");
			i += 2;
		}
		if (The_final_answer[i] == 'q')
		{
			The_final_answer.replace(i, 1, "cot");
			i += 2;
		}
		if (The_final_answer[i] == 'l')
		{
			The_final_answer.replace(i, 1, "ln");
			i += 1;
		}
		if (The_final_answer[i] == 'j')
		{
			The_final_answer.replace(i, 1, "sqrt");
			i += 3;
		}
	}
	if (makhsefr)
		cout << "denominator is zero :|  " << endl;
	else
		cout << The_final_answer << endl;
	system("pause");
	return 0;
}

//THE END
//Alireza Rezaeinia

//sin(x+sqrt(x^2))
//sin(x)/x
//SQRT(2x)
//sin(x)^2+cos(x)^2+(ln(x^2))+sin(x*tan(x))^2+cos(tan(x)*x)^2
//e^(2*x)
//2*32*x+24332432*x--x+x
//0.7*e^sin(e^65*cosln(e^454*x))
//ln((4*x)^2/(sin(x)*cos(8*x)))/sqrt((4*x)^3)
//cos(x^2+sin(x^2+2*x))*x
//tan(x+sin(x))
//cot(x^2)
//x+-x
//(x-(x-(x^2)))
//(x+(x+(x^6+2*x)))
//ln((4*x)^2/(sin(x)*cos(8*x)))/sqrt((4*x)^3)
//0.7*e^sin((e ^ 65)*cos(ln((e ^ 454)*x)))